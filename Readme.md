## Backend for basic transcript viewer
### The backend is python based with flask framework and uses mongodb as database

## Running backend server
```
#cd to directory
cd Backend

#create python virtual envirnoment
python3 -m venv backend_python

#activate python environment
source ./backend_python/bin/activate

#install required libraries from requirement.txt
pip install -r requirements.txt

#run backend code with
python backend.py

#A dev server will run at localhost:5000
```

## Running MongoDb
Mongo Db is used so we will need to run it as well.
We are doing this in docker.
```
#Any version of mongo will do.
docker run --name mongo_db_test -p 27017:27017 -d mongo:latest

#Default port is being used for connection. So, no further configuration is needed

#We can optionally create a unique index in 'roll' field for uniqueness.
db.student.createIndex({"roll":1},{unique:true})
```

## Populating data
There is json file with my current data, `test_student_info.json` which can be used to popuplate our mongo database.
```
#Save data
curl -X PUT -d @test_student_info.json http://127.0.0.1:5000/data

#response will contain the saved data + its id.

#check data by executing following get request
curl -X GET http://127.0.0.1:5000/data
```