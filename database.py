from pymongo import MongoClient

class DB(object):
    def __init__(self, host = 'localhost', port = 27017):
        self.host = host
        self.port = port
        client = MongoClient(self.host, self.port)
        self.db = client.results
    def find_one(self, filter = {}, projection = {'_id':0}):
        return self.db.student.find_one(filter, projection)

    def put_one(self, document):
        return self.db.student.insert_one(document)

    def update_one(self, filter, update):
        return self.db.student.update_one(filter, update)