from typing import Type
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from database import DB
from bson import ObjectId
import models

database = DB()

class DataLoader(Resource):
    #only returns first matching instance. DB in unique indexed with 'roll' field.
    def get(self):
        query = request.get_json() or {}
        data = database.find_one(query,{}) or {}
        if '_id' in data:
            data['id'] = str(data['_id'])
        return {key:data[key] for key in data if key != '_id'}, 200, {'Access-Control-Allow-Origin': '*'}

    #simply put whole doc into db after validation.
    def put(self):
        data = request.get_json(force=True)
        try:
            student = models.Student(data)
        except models.DataError as e:
            return {"success": False, "error": str(e)}, 400
        except Exception as e:
            return {"success": False, "error": str(e)}, 500

        try:
            student_data = student.get_data()
            database.put_one(student_data)
            if student_data and '_id' in student_data:
                student_data['id'] = str(student_data['_id'])
            return {"success": True, "data": {key:student_data[key] for key in student_data if key != '_id'}}, 200
        except Exception as e:
            return {"success": False, "error": str(e)}, 500
    
    #expected data :- filter: to find the document to update; document: document update expression;
    def post(self):
        data = request.get_json(force=True)
        filter = data.get('filter', None)
        document = data.get('document', None)
        if filter is None or document is None:
            return {"success":False,"error":"Data format not conformant"}, 400
        try:
            database.update_one(filter, document)
        except Exception as e:
            return {"success":False, "error":str(e)}, 500

        return {"success":True, "data": data}, 200



