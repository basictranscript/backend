from flask import Flask
from flask_restful import Resource, Api
from DataLoader import DataLoader

app = Flask(__name__)
api = Api(app)

api.add_resource(DataLoader, '/data')

if __name__ == '__main__':
    app.run(debug=True)