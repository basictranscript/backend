import json

class DataError(Exception):
    pass

class Models(object):
    def __init__(self, data):
        if set(data.keys()) !=  self.required_keys:
            raise DataError(f'Required datakeys missing or extra when creating {__class__}. Extra Keys are: {str(set(data.keys()) - self.required_keys)}. Missing Keys are {str(self.required_keys - set(data.keys()))}')
        try:
            self.init(**data)
        except TypeError as e:
            raise DataError('Data type Mismatch' + str(e))

class Student(Models):
    required_keys = set(['name','roll','year','faculty','level','semesters'])
    
    def __init__(self, data):
        super().__init__(data)

    def init(self, name:str, roll:str, year:int, faculty:str, level:str, semesters:list):
        self.name = str(name)
        self.roll = str(roll)
        self.year = int(year)
        self.faculty = str(faculty)
        self.level = str(level)
        self.semesters = [Semester(semester).get_data() for semester in semesters]
        if len({semester['number'] for semester in semesters}) != len([semester['number'] for semester in semesters]):
            raise DataError("Duplicate Semester Number")

    def get_data(self):
        return dict(
                name = self.name,
                roll = self.roll,
                year = self.year,
                faculty = self.faculty,
                level = self.level,
                semesters = self.semesters,
            )

    def get_json(self):
        return json.dumps(self.get_data())

class Semester(Models):
    required_keys = set(['number','subjects'])
    def __init__(self, data):
        super().__init__(data)
    
    def init(self, number:int, subjects: list):
        self.number = int(number)
        for i in range(0,len(subjects)):
            subjects[i]['id'] = i
        self.subjects = [Subject(subject).get_data() for subject in subjects]
        if len({subject['name'] for subject in subjects}) != len([subject['name'] for subject in subjects]):
            raise DataError("Duplicate subject Name")

    def get_data(self):
        return dict(
            number = self.number,
            subjects = self.subjects
        )
        
    def get_json(self):
        return json.dumps(self.get_data())

class Subject(Models):
    required_keys = set(['name','fm', 'pm', 'om', 'passedyear','code','id'])
    def __init__(self, data):
        super().__init__(data)

    def init(self, name:str, fm:float, pm:float, om:float, passedyear:int, code:str, id:int):
        self.name = str(name)
        self.fm = float(fm)
        self.pm = float(pm)
        self.om = float(om)
        self.passedyear = int(passedyear)
        self.code = str(code)
        self.id = int(id)

    def get_data(self):
        return dict(
                name = self.name,
                fm = self.fm,
                pm = self.pm,
                om = self.om,
                passedyear = self.passedyear,
                code = self.code,
                id = self.id
            )

    def get_json(self):
        return json.dumps(self.get_data())
